package net.tecgurus.pos.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import net.tecgurus.pos.model.Compra;
import net.tecgurus.pos.model.Compraproducto;
import net.tecgurus.pos.model.Producto;

@SpringBootTest
class ProductoServiceTest {
	
	@Autowired
	private ProductoService productoService;

	@Test
	@Transactional
	void testListar() {
		List<Producto> productos = productoService.listar();
		for (Producto producto : productos) {
			System.out.println(producto.getNombre());
			System.out.println(producto.getCategoria().getNombre());
		}
		//Compra compra = new Compra();
		//for(Compraproducto cp : compra.getCompraproductos()) {
			//String descripicion = cp.getProducto().getCategoria().getDescripcion();
		//}
	}

}
