package net.tecgurus.pos.service;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import net.tecgurus.pos.model.Categoria;

@SpringBootTest
class CategoriaServiceTest {
	
	@Autowired //Inyección de dependencias.
	private CategoriaService categoriaService;

	@Test
	void testGuardar() {
		Categoria categoria = new Categoria();
		categoria.setNombre("Categoria GG");
		categoria.setDescripcion("Descripcion GG");
		categoriaService.guardar(categoria); //INSERT 
		
		
		categoria.setNombre("Categoria Gerardo");
		System.out.println("ID Categoria: " + categoria.getIdcategoria());
		categoriaService.guardar(categoria); //UPDATE, por que viene con id
		
	}

	@Test
	void testListar() {
		List<Categoria> categorias = categoriaService.listar();
		for (Categoria categoria : categorias) {
			System.out.println(categoria.getNombre());
		}
	}

}
