package net.tecgurus.pos.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import net.tecgurus.pos.model.Usuario;


@SpringBootTest
class UsuarioServiceTest {
	
	@Autowired
	private UsuarioService usuarioService;

	@Test
	void testGuardar() {
		Usuario usuario = new Usuario();
		usuario.setNombre("Tec");
		usuario.setApaterno("Gurus");
		usuario.setEmail("vendedor@tecgurus.net");
		usuario.setPassword("password");
		usuario.setPerfil("USER");
		usuario.setActivo(true);
		usuarioService.guardar(usuario);
	}

}
