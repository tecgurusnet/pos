package net.tecgurus.pos;

import java.math.BigDecimal;
import java.util.Random;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import net.tecgurus.pos.model.Categoria;
import net.tecgurus.pos.model.Producto;
import net.tecgurus.pos.repository.CategoriaRepository;
import net.tecgurus.pos.repository.ClienteRepository;
import net.tecgurus.pos.repository.CompraRepository;
import net.tecgurus.pos.repository.CompraproductoRepository;
import net.tecgurus.pos.repository.ProductoRepository;
import net.tecgurus.pos.repository.UsuarioRepository;

@SpringBootTest
class RepositoriesTests {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private CompraRepository compraRepository;
	
	@Autowired
	private CompraproductoRepository compraproductoRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Test
	void dataLoad() {
		for (int i = 1; i <= 10; i++) {
			Categoria categoria = new Categoria();
			categoria.setNombre("Categoria " + i);
			categoria.setDescripcion("Descripcion " + i);
			categoriaRepository.save(categoria); // INSERT INTO
			
			for (int j = 1; j <= 10; j++) {
				Producto producto = new Producto();
				producto.setCategoria(categoria);
				producto.setNombre("Producto " + j + " cat " + i);
				producto.setSku("SKU " + j);
				producto.setDescripcion("Descripcion " + j);
				//Genero un stock aleatorio
				producto.setStock(new Random().ints(100, 1000).findFirst().getAsInt());
				//Genero precio aleatorio
				producto.setPrecio(BigDecimal.valueOf(new Random().doubles(10, 10000).findFirst().getAsDouble()));
				productoRepository.save(producto);  //INSERT INTO
			}
		}
	}

}
