package net.tecgurus.pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.tecgurus.pos.model.Compra;

@Repository
public interface CompraRepository extends JpaRepository<Compra, Integer>{

}
