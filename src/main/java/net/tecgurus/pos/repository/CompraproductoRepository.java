package net.tecgurus.pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.tecgurus.pos.model.Compraproducto;

@Repository
public interface CompraproductoRepository extends JpaRepository<Compraproducto, Integer>{

}
