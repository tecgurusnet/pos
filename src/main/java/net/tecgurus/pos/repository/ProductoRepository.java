package net.tecgurus.pos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.tecgurus.pos.model.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Integer>{
	@Query("SELECT p FROM Producto p WHERE p.nombre LIKE %:busqueda% OR p.descripcion LIKE %:busqueda% OR p.sku LIKE %:busqueda%" )
	public List<Producto> findByNombreOrDescripcionOrSku(@Param("busqueda") String busqueda);
}
