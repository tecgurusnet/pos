package net.tecgurus.pos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import net.tecgurus.pos.dto.InventarioDTO;

public interface InventarioRepository extends JpaRepository<InventarioDTO, Integer>{

	@Query(value = "CALL CONSULTA_INVENTARIO_EMM();", nativeQuery = true)
	public List<InventarioDTO> consultarInventario();
}
