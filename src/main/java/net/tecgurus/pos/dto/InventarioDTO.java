package net.tecgurus.pos.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;

@NamedStoredProcedureQuery(
		name= "CONSULTA_INVENTARIO_EMM",
		procedureName = "CONSULTA_INVENTARIO_EMM",
		resultClasses = InventarioDTO.class
)
@Entity
public class InventarioDTO {
	@Id
	private int idproducto;
	
	private String nombre;
	private double ventastotales;
	private long cantidadvendidos;
	private long stock;
	public int getIdproducto() {
		return idproducto;
	}
	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getVentastotales() {
		return ventastotales;
	}
	public void setVentastotales(double ventastotales) {
		this.ventastotales = ventastotales;
	}
	public long getCantidadvendidos() {
		return cantidadvendidos;
	}
	public void setCantidadvendidos(long cantidadvendidos) {
		this.cantidadvendidos = cantidadvendidos;
	}
	public long getStock() {
		return stock;
	}
	public void setStock(long stock) {
		this.stock = stock;
	}
	
	
	

	
}
