package net.tecgurus.pos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.tecgurus.pos.model.Producto;
import net.tecgurus.pos.service.ProductoService;

@RestController
@RequestMapping("/producto")
public class ProductoController {

	@Autowired
	private ProductoService productoService;
	
	@GetMapping
	public List<Producto> listar() {
		return productoService.listar();
	}
	
	@GetMapping("/busqueda/{param}")
	public ResponseEntity<List<Producto>> buscar(@PathVariable("param") String criterio) {
		List<Producto> productos =  productoService.buscar(criterio);
		return ResponseEntity.ok(productos);
	}
	
	
}
