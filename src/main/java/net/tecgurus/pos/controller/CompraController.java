package net.tecgurus.pos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.tecgurus.pos.model.Compra;
import net.tecgurus.pos.service.CompraService;

@RestController
@RequestMapping("compra")
public class CompraController {
	
	@Autowired
	private CompraService compraService;

	
	@PostMapping
	public ResponseEntity<?> insertar(@RequestBody Compra compra) {
		Compra insertada = compraService.guardar(compra);
		return ResponseEntity.ok(insertada);
	}
	
	@GetMapping
	public List<Compra> listar() {
		return compraService.listar();
	}
	
	//Tarea, eliminar compra por id, actualizar una compra, buscar una compra por rango de fechas.
}
