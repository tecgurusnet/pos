package net.tecgurus.pos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import net.tecgurus.pos.dto.InventarioDTO;
import net.tecgurus.pos.service.InventarioService;

@RestController
@RequestMapping("/compra/inventario")
@Api(value = "Endpoint de resumen de inventarios")
public class InventarioController {
	
	@Autowired
	private InventarioService inventarioService;
	
	
	@GetMapping
	public List<InventarioDTO> consultarInventario() {
		return inventarioService.consultarInventario();
	}
}
