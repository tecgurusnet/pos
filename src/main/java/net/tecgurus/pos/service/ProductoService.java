package net.tecgurus.pos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.tecgurus.pos.model.Producto;
import net.tecgurus.pos.repository.ProductoRepository;

@Service
public class ProductoService {
	
	@Autowired
	private ProductoRepository productoRepository;

	@Transactional(readOnly = true)
	public List<Producto> listar() {
		return productoRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public List<Producto> buscar(String busqueda) {
		return productoRepository.findByNombreOrDescripcionOrSku(busqueda);
	}
}
