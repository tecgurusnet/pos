package net.tecgurus.pos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.tecgurus.pos.dto.InventarioDTO;
import net.tecgurus.pos.repository.InventarioRepository;

@Service
public class InventarioService {

	@Autowired
	private InventarioRepository inventarioRepository;
	
	@Transactional(readOnly = true)
	public List<InventarioDTO> consultarInventario() {
		return inventarioRepository.consultarInventario();
	}
}
