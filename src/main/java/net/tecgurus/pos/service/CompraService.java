package net.tecgurus.pos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.tecgurus.pos.model.Compra;
import net.tecgurus.pos.model.Compraproducto;
import net.tecgurus.pos.repository.CompraRepository;
import net.tecgurus.pos.repository.CompraproductoRepository;

@Service
public class CompraService {
	
	@Autowired
	private CompraRepository compraRepository;
	
	@Autowired
	private CompraproductoRepository compraProductoRepository;
	
	@Transactional
	public Compra guardar(Compra compra) {
		
		Compra compraActualizada = compraRepository.save(compra); //idcompra 23,24,25.....9999
		
		for(Compraproducto compraProducto : compra.getCompraproductos()) {
			compraProducto.setCompra(compraActualizada);
			compraProductoRepository.save(compraProducto);
		}
		
		return compraActualizada;
	}
	
	
	@Transactional(readOnly = true)
	public List<Compra> listar() {
		return compraRepository.findAll();
	}

}
