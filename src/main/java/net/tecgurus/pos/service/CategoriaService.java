package net.tecgurus.pos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.tecgurus.pos.model.Categoria;
import net.tecgurus.pos.repository.CategoriaRepository;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Transactional
	public void guardar(Categoria categoria) { //begin transaction
		//Si trae valor en el idCategoria (PK) va a actualizar UPDATE
		//Si no trae valor (es decir viene null) va a insertar INSERT
		categoriaRepository.save(categoria); 
	} //commit transaction
	
	@Transactional(readOnly = true)
	public List<Categoria> listar() {
		return categoriaRepository.findAll();
	}
}
