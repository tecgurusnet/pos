package net.tecgurus.pos.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import net.tecgurus.pos.security.entrypoint.JwtAuthenticationEntryPoint;
import net.tecgurus.pos.security.filter.JwtRequestFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	
	@Autowired
	private UserDetailsService userDetailService;
	
	@Autowired
	private JwtRequestFilter jwtRequestFilter;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	private static final String[] AUTH_WHITELIST = {
			"/login",
			"/v2/api-docs",
			"/swagger-resources",
			"/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
	};
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailService).passwordEncoder(passwordEncoder);
	}
	
	
	
	
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//CORS intercambio de recursos de origen cruzados
		http.cors().and().authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/**").permitAll();
		//Deshabilitar el csrf, falsificacion de peticion en sitios cruzados
		http.csrf().disable()
		//Voy a permitir que deje pasar las URL del whitelist
		.authorizeRequests().antMatchers(AUTH_WHITELIST).permitAll().
		
		antMatchers("/compra/inventario").hasAuthority("ADMIN").
		//antMatchers("/usuario/**","/catalogos/**").hasAnyAuthority("ADMIN","SUPERADMIN").
		//Cualquier petición autenticada dejala pasar.
		anyRequest().authenticated().and()
		//Si ocurre algun error de seguridad, el entrypoint regresará un 401 http
		.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
		//No se guarda el estado de la sesion.
		.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); 
		
		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
}
