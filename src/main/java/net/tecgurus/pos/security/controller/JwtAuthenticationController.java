package net.tecgurus.pos.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import net.tecgurus.pos.security.request.LoginRequest;
import net.tecgurus.pos.security.service.JwtUserDetailsService;
import net.tecgurus.pos.security.util.JwtTokenUtil;

@RestController
public class JwtAuthenticationController {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private JwtUserDetailsService jwtUserDetailService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@PostMapping("/login")
	public String login(@RequestBody LoginRequest request) {
		try {
			authenticate(request.getUsername(), request.getPassword());
			
			UserDetails userDetails = jwtUserDetailService.loadUserByUsername(request.getUsername());
			String token = jwtTokenUtil.generateToken(userDetails);
			return token;
			
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	
	private void authenticate(String username, String password) {
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
	}
}
