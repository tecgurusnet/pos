package net.tecgurus.pos.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import net.tecgurus.pos.security.service.JwtUserDetailsService;
import net.tecgurus.pos.security.util.JwtTokenUtil;

@Component
//Por cada petición (request) atrapa la peticion (header - token)
public class JwtRequestFilter extends OncePerRequestFilter {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private JwtUserDetailsService jwtUserDetailsService;

	//Se sobreescribe para obtener el header Authorization y con base a ese header leer el token JWT.
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String requestTokenHeader = request.getHeader("Authorization");
		String jwtToken = null;
		String username = null;
		
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			//Quitarle la palabra Bearer
			jwtToken = requestTokenHeader.substring(7);  //Omite los primeros 7 caracteres = Bearer 
			username = jwtTokenUtil.getUsernameFromToken(jwtToken);
		} else {
			System.out.println("Token no es de tipo JWT");
		}
		
		//Que el username no sea nulo y que este ejecutando correctamente el contexto de spring security
		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			//Si el usuario existe en la tabla Usuario
			UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);
			if(jwtTokenUtil.validateToken(jwtToken, userDetails)) {
				//Se crea el objeto que indica a spring security que el usuario y  token son validos.
				UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				//Si trae información adicional en el header lo guardo para reenviarlo.
				upat.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				
				SecurityContextHolder.getContext().setAuthentication(upat);
			}
		}
		filterChain.doFilter(request, response); //Ve al controller que te solicitaron
	}
	
	
}
